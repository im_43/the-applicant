<div class="dialogue">
    <p> You jolt awake. The voice from the dream still whispers, ever so faintly. And now it falls silent. </p>
    <p> You look around, shapes unfocused into vague blobs. Your ears are ringing. Was the explosion you heard part of your dream or was it what awoke you? You rub your eyes. </p>
    <p> A voice reverberates through a poorly lit corridor. It's yelling. </p>
    <p> You stand up. And notice a small robot approaching you. </p>
    <p> <strong>"IDENTIFY YOURSELF"</strong>, it demands. </p>
    <p> You stare at it in confusion, sleep still numbing your senses. </p>
    <p> <strong>"PLEASE PROVIDE YOUR APPLICANT IDENTIFICATION CODENAME AND YOUR AUTHORIZATION PASSWORD"</strong>. It waves what seems to be a weapon. </p>
</div>

<div class="response">
    <form id="login" action="login.php" method="post">
        <fieldset class="form-group drkdiv">           
            "I am Applicant&nbsp;
            <input autofocus autocomplete="off" name="username" placeholder="Username" type="text"/>
            ",&nbsp;and my password is&nbsp;
            <input name="password" placeholder="Password" type="password"/>
            ," you think to yourself.&nbsp;<button type="button" onclick="checker('login');" class="drkbutton">And then you say it.</button>
        </fieldset>
    </form>
    <form action="register.php" method="get">
        <fieldset class="form-group">
            <button type="submit" class="drkbutton">"I have no idea what you're talking about."</button>     
        </fieldset>
    </form>
</div>