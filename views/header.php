<!DOCTYPE html>

<html>

    <head>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link href="/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/css/bootstrap-theme.min.css" rel="stylesheet"/>
        <link href="/css/avgrund.css" rel="stylesheet"/>
        <link href="/css/simple-sidebar.css" rel="stylesheet"/>
        <link href="/css/styles.css" rel="stylesheet"/>

        <?php if (isset($title)): ?>
            <title><?= htmlspecialchars($title) ?></title>
        <?php else: ?>
            <title>The Applicant</title>
        <?php endif ?>

        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="./js/jquery.avgrund.js"></script>
        <script src="/js/scripts.js"></script>

    </head>

    <body>

        <div class="container">