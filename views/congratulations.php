<div class="dialogue">
    <p> You follow the Application Associate in silence down a long corridor and into what appears to be a waiting room. </p>
    <p> One of the chairs there is occupied by a person. He seems to be asleep, a fine trail of drool running down his cheek. </p>
    <p> "This is your first assignment", the Associate proclaims. </p>
    <p> You give it a puzzled look. </p>
    <p> "Your job is to guide that Applicant through the Application Process. I'm sure you will know what to do." </p>
    <p> "W...what?" you manage to utter. </p>
    <p> "That's right, you have proven your worth, and now the job of Application Associate is yours." The robot makes a small pause. "However, that also means my assignment is also complete." Its voice sounds triumphant. It turns towards the pyramid. </p>
    <p> "What now, Master ABYSS? What happens next?" </p>
    <p> The pyramid stares back, impassive. </p>
    <p> "Please, tell me what my next assignment..." The robot trails off.</p>
    <p> And you witness in horror as the pyramid's eye turns red. The robot's voice now sounds terrified. </p>
    <p> "No, it can't. It can't be," it mutters, stuttering. And then it turns to you. "You... you are..." It turns to the sleeping applicant. "No, this cannot be true. I refuse to believe this." Its voice grows louder and louder. It now bolts down the corridor you came from. You find yourself following it. </p>
    <p> The robot is now staring at the button. </p>
    <p> "All of this... But the paradox... OH, GOD, NO." </p>
    <p> The robot's head suddenly begins to give off sparks, and is now spinning seemingly out of control. There are a couple of loud bangs coming from its body, and the smoke begins to fill the room. It's screaming, and the sound makes you cringe in fear. You prepare for an explosion. </p>
    <p> But now the pyramid is looking at you, and you feel your memory begin to slip away from you, as if it were just sand running away through your clutching fingers. </p>
    <p> You grab your head in an attempt to stop whatever the pyramid is doing to you, and look around in an attempt to understand. And then you see it, and suddenly the terrifying realization hits you. </p>
    <p> You are looking at the robot, only it's not there in the room with you, head spinning, about to explode. It's in the mirror. </p>
    <p> After all the enhancements you did to your body, you look exactly like the robot who greeted you when you woke up so long ago in the strange waiting room of the strange facility. That is also why you didn't recognize your own face just a moment ago in the person sleeping placidly on the chair, because it hasn't been yours anymore for some time. You were so focused on the goal, that you lost yourself in the process. </p>
    <p> As the last few memories slip away to give room to the Application Associate's set of instructions, you watch your future self explode with a loud bang. </p>
    <p> And then it all turns black. </p>
</div>

<div class="response">
    <form action="congratulations.php" method="post">
        <fieldset class="form-group">           
            <button type="submit" class="drkbutton">Continue...</button>
        </fieldset>
    </form>