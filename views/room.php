<div class="invdialogue">
	<p class="inventory"><?= $intro ?>...</p>
	<div>
		<?php 
			
			// if there are no items, say there's nothing
			if (empty($items)) {
				echo "..." . $empty;
			}

			// display each of the items
			foreach ($items as $item) {
				echo "<p>" . $item["invdesc"] . " It gives you <strong>" . $item["amt"] . "</strong> extra points every <strong>" . $item["offset"] . "</strong> button presses. </p>";
			}
		?>
	</div>
</div>