<div class="dialogue">
    <p> The robot stares at you for what seems to be an eternity. </p>
    <p> And then you hear an alarm blaring through its speakers. </p>
    <p> <strong>"ALL UNAUTHORIZED PERSONNEL WILL BE VAPORIZED AND VACUUMED INTO THE SAUNA AREA FOR THE ENJOYMENT
    OF AUTHORIZED PERSONNEL,"</strong> it yells, pointing the weapon at you.</p>
    <p> You feel a chill run down your spine. </p>
    <p> <strong>"Wait!"</strong>, you hear yourself say. <strong>"I'm here for the job."</strong></p>
    <p> There is a short silence. The palms of your hands feel increasingly moist.</p>
    <p> <strong>"Oh,"</strong> it says. The alarm falls silent, its weapon retracts, and its other arm extends, holding what seems to be a clipboard.
    <strong> "Please state your Applicant Identification Codename and Authorization Password of choice so you can be entered into the system.
    Then we'll proceed to the evaluation process."</strong></p>
    <p> You frown in confusion. <strong>"Applicant Identification Codename? Authorization Password?"</strong></p>
    <p> The robot remains silent, seemingly awaiting an answer. The dead red light of its optic seems to pierce through your face.</p>
</div>

<div class="response">
    <form id="register" action="register.php" method="post">
        <fieldset>
            <div class="form-group drkdiv">
                "OK, my Applicant Identification Codename is&nbsp;
                <input autofocus name="username" placeholder="Username" type="text"/>
                , and my Authorization Password is&nbsp;
                <input name="password" placeholder="Password" type="password"/>
                . That is spelled: '
                <input name="confirmation" placeholder="Password" type="password"/>
                ,'" <button type="button" onclick="checker('register')" class="drkbutton">you say.</button>
            </div>
        </fieldset>
    </form>
    <form action="login.php" method="get">
        <fieldset>
            <button type="submit" class="drkbutton">"Wait... I remember this... This has already happened bef--"</button>     
        </fieldset>
    </form>
</div>


