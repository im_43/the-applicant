<!-- Sidebar -->
<div id="wrapper">

    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand" style="color: #999999;">
                Actions
            </li>
            <li>
                <a href="/">Look at the button</a>
            </li>
            <li>
                <a href="/mirror.php">Look at the mirror</a>
            </li>
            <li>
                <a href="/room.php">Look around</a>
            </li>
            <li>
                <a href="/associate.php">Look at Supervision Associate</a>
            </li>
            <li>
                <a href="/abyss.php">Look at ABYSS</a>
            </li>
            <li>
                <a href="/shop.php">Open shop interface</a>
            </li>
            <li>
                <a href="/logout.php">Fall Asleep</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->