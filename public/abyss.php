<?php

	/* Controller for ABYSS image page */

	// loads configuration
	require("../includes/config.php");

	// renders ABYSS image with sidebar
	render("/abyss.php", 2, ["title" => "A mountain walked or stumbled."]);

?>