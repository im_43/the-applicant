<?php

    /* Controller for register page. Registers new users.*/

    // loads configuration
    require("../includes/config.php");
    
    // if user reached page via GET
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
    
    // renders form
    render("register_form.php", 3, ["title" => "Welcome, Applicant"]);
    }
    
    // else if user reached page via POST
    else if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        // checks for errors inserting user into the database
        if (query("INSERT INTO users (users, hash, clicks) VALUES(?, ?, 0)", $_POST["username"], crypt($_POST["password"])) === false) {
            
            // display error message
            echo "DATABASE ERROR. GO BACK AND TRY AGAIN.";
        }
        
        // if there are no errors
        else {
            
            // updates session id with database id and redirects to index
            $rows = query("SELECT LAST_INSERT_ID() AS id");
            $id = $rows[0]["id"];
            $_SESSION["id"] = $id;
            redirect("associate.php");
        }
    }
?>
