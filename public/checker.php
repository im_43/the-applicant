<?php
	
	/* AJAX checker user input.

	For the shop page, checks whether a user already has an item when attempting to buy one
	or whether they can afford it and returns random error messages
	from an array of dialogue if item is owned or too expensive.

	For the login and register pages, checks for empty fields, correct password,
	non-existent user for login, duplicate users for register, and unmatching password
	and confirmation password for register.

	Returns JSON. */
	
	// loads configuration
    require("../includes/config.php");

    // determines type of check
    $type = $_POST["checktype"];

    switch ($type) {

    	// if checking for shop transaction
		case "shop":
		    
		    // error messages for when user can't afford item
		    $affordmsg = [
				"We are sorry to announce that you are too poor for this item.",
				"Not enough Job Points. Please go back to the Button and click some more.",
				"Too expensive. Please keep the weeping at a minimum.",
				"The \"price\" column is there for a reason, you know?",
				"HAHAHAHAHAHAHA! Oh, you're serious. Well, you just can't afford that.",
				"Currently, we do not accept or support imaginary currency.",
				"Yeah, you wish.",
				"Clearly, you fail to understand the dynamics of basic economic transactions."
			];

			// error messages for when user already owns item
			$ownedmsg = [
				"You already own this item.",
				"Well, if you wanna give me the points for it again...",
				"Haven't you bought that already?",
				"Unfortunately we're out of stock for that. We had only one. And YOU bought it.",
				"Ooh! Free points!"
			];

			// picks random error message and stores it in variable
			$data = ["msgafford" => $affordmsg[array_rand($affordmsg)], "msgowned" => $ownedmsg[array_rand($ownedmsg)]];
		
		break;

		// if checking login input
		case "login":

			// if this variable is not modified, process was successful
			$status = "success";
			// empty content string
			$dialogue = "";

			// if both username and password fields are empty
			if (empty($_POST["username"]) && empty($_POST["password"])) {
				
				// updates error status for debugging purposes
				$status = "emptyall";
				// updates content
				$dialogue = "<p>You stutter so badly neither Codename nor Password came out at all. The robot begins to charge its weapon.</p>\n<p><strong>\"You have failed to provide identification. Identify yourself or be anihilated.\"</strong></p>\n<p><strong>You quickly try again.</strong></p>";
			}

			// if only username field is empty
			else if (empty($_POST["username"])) {
				
				// updates error status for debugging purposes
				$status = "emptyuser";
				// updates content
				$dialogue = "<p>You realize you completely forgot to remember your Applicant Identification Codename but still attempted to say what was on your mind, and thus awkwardly ended up uttering an incomplete sentence. The robot waves the weapon angrily.</p>\n<p><strong>\"You have failed to provide an Applicant Identification Codename. Provide a valid Applicant Identification Codename or be obliterated.\"</strong></p>\n<p><strong>You try again.</strong></p>";	
			}

			// if only password field is empty
			else if (empty($_POST["password"])) {
				
				// updates error status for debugging purposes	
				$status = "emptypass";
				// updates content
				$dialogue = "<p>You realize you did not say your Authorization Password at all. The robot is not amused.</p>\n<p><strong>\"You have failed to provide an Authorization Password. Provide a valid Authorization Password or be exterminated.\"</strong> It seems almost ready to fire its weapon at you.</p>\n<p><strong>You try again.</strong></p>";	
			}

			// if all fields are complete
			else {
				
				// query database for user
		        $rows = query("SELECT * FROM users WHERE users = ?", $_POST["username"]);

		        // if there's no such user
		        if (count($rows) != 1) {
		            // updates error status for debugging purposes
		            $status = "usrerror";
		        }    
		        
		        // if we found user, check password
		        else {
			        
			        // first (and only) row
			        $row = $rows[0];
		        	
		        	// compare hash of user's input against hash that's in database
		        	if (crypt($_POST["password"], $row["hash"]) != $row["hash"]) {

		        		// updates error status for debugging purposes
		        		$status = "passerror";
		        	}
		        }

		        // if there's no such user or the password is wrong
		        if ($status == "usrerror" || $status == "passerror") {

		        	// update content
		        	$dialogue = "<p><strong>\"You do not appear in our records,\"</strong> the robot says. <strong>\"Get ready for immediate decimation.\"</strong></p>\n<p><strong>You quickly try again.</strong></p>";
		        }				
			}

			// stores status and content in variable
			$data = ["status" => $status, "dialogue" => $dialogue];

		break;

		// if checking register input
		case "register":

			// if this variable is not modified, process was successful
			$status = "success";
			// empty content string
			$dialogue = "";

			// if all three fields are empty
			if (empty($_POST["username"]) && empty($_POST["password"]) && empty($_POST["confirmation"])) {
				
				// updates error status for debugging purposes
				$status = "emptyall";
				// updates content
				$dialogue = "<p>The alarm begins to blare again.</p>\n<p><strong>\"You have failed to provide your registration data. In fact, you're barely capable of producing coherent sentences. Prepare for prompt extermination.\"</strong></p>\n<p><strong>You quickly try again.</strong></p>";
			}

			// if only username field is empty
			else if (empty($_POST["username"])) {

				// updates error status for debugging purposes
				$status = "emptyuser";
				// updates content
				$dialogue = "<p>You realize you completely forgot to remember your Applicant Identification Codename but still attempted to say what was on your mind, and thus awkwardly ended up uttering an incomplete sentence. The robot is unamused.</p>\n<p><strong>\"You have failed to provide an Applicant Identification Codename. Provide a valid Applicant Identification Codename or be obliterated.\"</strong></p>\n<p><strong>You try again.</strong></p>";	
			}
			
			// if only password field is empty
			else if (empty($_POST["password"])) {

				// updates error status for debugging purposes
				$status = "emptypass";
				// updates content
				$dialogue = "<p>You realize you did not mention an Authorization Password at all. The robot seems irritated.</p>\n<p><strong>\"You have failed to provide an Authorization Password. Provide a valid Authorization Password or be exterminated.\"</strong> Its weapon arm trembles, ever so slightly, as if it were about to point it at you again.</p>\n<p><strong>You try again.</strong></p>";	
			}

			// if only confirmation field is empty
			else if (empty($_POST["confirmation"])) {
				
				// updates error status for debugging purposes
				$status = "emptyconf";
				// updates content
				$dialogue = "<p>You realize you failed to spell your Authorization Password at all.</p>\n<p><strong>\"Please spell your Authorization Password so it can be entered correctly into the Applicants Database. Also, please, disregard the 'please' in the previous sentence. Do not construe it as advice. It was a direct order under penalty of death.\"</strong> It does not appear to be joking.</p>\n<p><strong>You try again.</strong></p>";	
			}

			// if password doesn't math password confirmation field
			else if ($_POST["password"] != $_POST["confirmation"]) {
				
				// updates error status for debugging purposes
				$status = "nomatch";
				// updates content
				$dialogue = "<p>You realize your spelling capabilities have greatly diminished since your glory days as Spelling Bee Olympic Gold Medallist back in elementary school, and you completely failed to spell your Authorization Password correctly. The robot judges expressionlessly.</p>\n<p><strong>\"Please spell your Authorization Password correctly so we can enter you in the Applicants Database. Repeated failure to do so will result in an extra clause in your potential contract requiring your to wear a dunce hat for the duration of your duties.\"</strong></p>\n<p>You fear for your moral integrity and <strong>try again.</strong></p>";	
			}

			// if all fields are complete
			else {

				// query database for user
		        $rows = query("SELECT * FROM users WHERE users = ?", $_POST["username"]);

		        // if user is already in database (username is taken)
		        if (count($rows) == 1) {
		            
		            // updates error status for debugging purposes
		            $status = "loginerror";
		            // updates content
		            $dialogue = "<p><strong>\"You seem to already be in our database,\"</strong> the robot says with a hint of irritation. <strong>\"Impersonation of existing employees is considered a crime against our Company's policy, and is punishable by flaying.\"</strong></p>\n<p><strong>\"HEH! I MEANT...\",</strong> you say, <strong>and promptly try again.</strong></p>";
		        }    				
			}

			// stores status and content in variable
			$data = ["status" => $status, "dialogue" => $dialogue];

		break;
	}

	// outputs data as JSON (pretty-printed for debugging convenience)
    header("Content-type: application/json");
    print(json_encode($data, JSON_PRETTY_PRINT));
?>