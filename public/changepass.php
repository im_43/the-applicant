<?php

    /* Controller for Password Change page.
    This is just backend implementation. Password change is not currently supported,
    as there is no front-end implementation yet. */

    // loads configuration
    require("../includes/config.php"); 

    // if user reached page via GET
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        
        // render form
        render("changepass_form.php", ["title" => "Change password"]);
    }

    // else if user reached page via POST
    else if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        // stores old pass hash from database and gets rid of empty arrays
        $oldpass = query("SELECT hash FROM users WHERE id = ?", $_SESSION["id"]);
        $oldpass = $oldpass[0]["hash"];

        /* USER INPUT VALIDATION */

        // wrong password
        if ($oldpass != crypt($_POST["oldpass"], $oldpass)) {
            echo "Incorrect password :( Please type your old password again.";
        }

        // no new password
        else if (empty($_POST["newpass"])) {
            echo "You must provide a new password! >:(";
        }

        /* END OF USER INPUT VALIDATION*/
        
        // everything 200 OK
        else {
            
            // updates database's hash with new password hash
            query("UPDATE users SET hash = ? WHERE id = ?", crypt($_POST["newpass"]), $_SESSION["id"]);
        }

        // renders confirmation view
        render("changepass.php", ["title" => "Password changed successfully!"]);
    }

?>
