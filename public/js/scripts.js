/*
 * "The Applicant" by Lucas Traba
 * Scripts
 */


// on document load
$(function(){

	// store current page's path
	var location = window.location.pathname;

	// initialize sidebar hiding button
  	$("#menu-toggle").click(function(e) {
	    e.preventDefault();
	    
	    // toggle sidebar class to hide/show
	    $("#wrapper").toggleClass("toggled");
	    
	    // change sidebar toggle button text
	    var text = $('#menu-toggle').text();	    
	    $("#menu-toggle").text(
	        text == "[Sit forward]" ? "[Sit back]" : "[Sit forward]"
        );
    });

	// if page is the shop
	if (location == "/shop.php") {
		
		// declare variable for error message 
		var affordmsg;
	}

	// if page is the index
	if (location == "/") {

		// we are going to load the clicks in the counter page	
		var parameters = {
			type: "load"
		};
		
		// JSON post request that loads the amount of clicks
		$.post("counter.php", parameters)
	    
	    // when data is received
		.done(function(data, textStatus, jqXHR) {
	    
	  		// set UI clicks to database clicks
	  		$("#clicks").text(formatNumber(data[0]["clicks"]));	

	    });
    }

    // if page is the associate page
    if (location == "/associate.php") {

    	// set parameters for AJAX request
    	var parameters = {
    		option: "default"
    	}

		// posts parameters to dialogue.php to obtain dialogue content and responses
		$.post("dialogue.php", parameters)

		// if the request is successful
		.done(function(data, textStatus, jqXHR) {

			// update dialogue with returned dialogue
	    	$(".dialogue").html(data.dialogue);
	    	// slice the response object into an array
	    	var response = Array.prototype.slice.call(data.response);
	    	// update response section with returned responses, each in a new line
	    	$(".response").html(response.join("\n"));
	    })

		// if request fails
		.fail(function(jqXHR, textStatus, errorThrown) {

	        // log error to browser's console
	        console.log(errorThrown.toString());
	    });	
    }
})


/*
 * Updates clicks when user clicks button
 */
function counter() {

    // we are going to have the counter page "sum" a click
    var parameters = {
    	type: "sum"
    }    

    // JSON post request that sums a click in the DB and displays it
	$.post("counter.php", parameters)
    
    // when data is received
    .done(function(data, textStatus, jqXHR) {
    
    	// update UI clicks to updated DB clicks
    	$( "#clicks" ).text(formatNumber(data[0]["clicks"]));
    });

    // toggles button image for button animation
    $('#button img').attr('src', "img/button-dn.png");

    // after 150 milliseconds returns to original button image
    setTimeout(function(){
    	$('#button img').attr('src', "img/button-up.png");
    }, 150);
}


/*
 * Handles purchases made at the shop. The function requires the item ID and the price,
 * which have already been queried when displaying the page.
 */
function purchase(id, price, clicks, status) {

	/* CHECKING */

	// variable initialization
	var affordmsg = "";
	var ownedmsg = "";

	// sets the type of check to shop
	var parameters = {

		checktype: "shop"
	};

	// JSON post request that handles messages
	$.post("checker.php", parameters)

	// if successful
	.done(function(data, textStatus, jqXHR) {
		
		// update variables with obtained data
		affordmsg = data.msgafford;
		ownedmsg = data.msgowned;

		// if the item is owned, display the "already owned" message
		if (status == "owned") {

			$("#affordtgl").text(ownedmsg);
		}

		// if the item is too expensive for the user, display the "can't afford" message
		else if (price > clicks) {

			$("#affordtgl").text(affordmsg);
		}
    })

	// if request fails
	.fail(function(jqXHR, textStatus, errorThrown) {

        // log error to browser's console
        console.log(errorThrown.toString());
    });

	// if the item is already owned or is too expensive, don't proceed with the purchase
	if (status == "owned" || price > clicks) {

		return;
	}

	/* PURCHASE */

	// sets the parameters for JSON request
	parameters = {
		
		purchase: true,
		id,
		price
	};

	// JSON post request that handles purchases
	$.post("shop.php", parameters);
	
	// reloads page to make sure shop page is up to date
	location.reload();
	
}

/* Handles dynamic dialogue */
function dialogue(option)
{	

	// establishes the option chosen by the user
	var parameters = {
		option: option
	}

	// posts via AJAX to dialogue.php the parameters
	$.post("dialogue.php", parameters)

	// if successful
	.done(function(data, textStatus, jqXHR) {

		// if a redirect flag is issued, redirect
		if (typeof data.redirect !== 'undefined') {
			window.location.replace("/");
		}

		// if an endgame flag is issued, take the user to the endgame page
		if (typeof data.endgame !== 'undefined') {
			window.location.replace("/congratulations.php");
		}

		// display the dialogue
    	$(".dialogue").html(data.dialogue);
    	// slice the response object into an array
    	var response = Array.prototype.slice.call(data.response);
    	// display the responses array, each in a new line
    	$(".response").html(response.join("\n"));
    })

	// if request fails
	.fail(function(jqXHR, textStatus, errorThrown) {

        // log error to browser's console
        console.log(errorThrown.toString());
    });
}

/* Checks integrity of user input in login and registration fields */
function checker(type) {
	
	// establishes parameters
	var parameters = {
		checktype: type,
		username: $("input[name='username']").val(),
		password: $("input[name='password']").val() 
	};

	// if it's a registration, get the password confirmation field
	if (parameters.checktype == "register") {
		parameters.confirmation = $("input[name='confirmation']").val();
	}
	
	// JSON post request that handles messages
	$.post("checker.php", parameters)

	// if successful
	.done(function(data, textStatus, jqXHR) {

		// if everything is fine, submit the form
		if (data.status == "success") {
			$("#login, #register").submit();
		}

		// if something is missing, display the error message
		else {
			$(".dialogue").html(data.dialogue);
		}
    })

	// if request fails
	.fail(function(jqXHR, textStatus, errorThrown) {

   		// log error to browser's console
    	console.log(errorThrown.toString());
	});
}

/* Adds appropriate commas to numbers larger than 999. */
function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

// groundwork for future ABYSS animation events
/*
function abyssloader() {


} */