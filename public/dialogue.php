<?php
	
	/* AJAX dialogue handler. Handles branching dialogue and dynamically changes
	content both randomly and depending on user's chosen options and current stage. */

	// loads configuration
    require("../includes/config.php");

    // gets user's current stage and gets rid of extra array
    $stage = query("SELECT stage FROM users WHERE id = ?", $_SESSION["id"]);
    $stage = $stage[0]["stage"];

    // gets user's current clicks and gets rid of extra array
    $clicks = query("SELECT clicks FROM users WHERE id = ?", $_SESSION["id"]);
    $clicks = $clicks[0]["clicks"];

    // gets the option picked by the user from POST
    $option = $_POST["option"];

    // initializes variables
    $dialogue = "";
    $response = [];
    $tutend = 0;

    // initializes array with content for the help hub
    $hlplines = [
    	"\"All doubts will be swiftly exterminated. As will you if you don't stop having them soon.\"",
    	"\"INSERT QUESTION HERE.\"",
    	"\"My programming says that in the event that you have questions I should advice you to please refrain from having questions, but you look particularly dumb, so I will make an exception for now.\"",
    	"\"What? What is it now?\"",
    	"\"How may I reluctantly help you?\"",
    	"\"How can I be of service to you, puny human?\"",
    	"\"How can I help decrease, albeit asymptotically, your infinite human ignorance?\"",
    	"\"Oh, the human does not understand. Now, that's a new one.\"",
    	"\"The human is confused. How adorable.\"",
    	"\"Do I look like an encyclopedia? Because the reassembly machine wasn't at its best this morning.\"",
    	"\"Do you hate me? Is that it?\"",
    	"\"To inform or not to inform, that is the question.\"",
    	"\"It's just pressing a button, you know? What can possibly be so confusing about that?\"",
    	"\"I would say it was the buttler with the candlestick in the kitchen.\"",
    	"\"I do not get paid enough for this. As a matter of fact, I do not get paid at all.\"",
    	"\"Again? Seriously?\"",
    	"\"We've already gone through this before.\""
    ];

    // initializes variable with content for the "back" button
    $backbtn = [
    	"\"I have some questions.\"",
    	"\"About the Application process...\"",
    	"\"I'm not sure I understand this whole thing.\"",
    	"\"Mind if I ask you some questions?\"",
    	"\"Bear with me for a moment, the learning curve is a bit too steep.\"",
    	"\"Um...\"",
    	"\"Regarding this whole thing...\"",
    	"\"I need some clarification about some stuff.\"",
    	"Stare in confusion.",
    	"Blank stare."
	];

	// if the user is still in the tutorial stage
	if ($stage == 0) {

		// sets specific "back" button content
		$backbtn = [
			"\"Uh... Okay. I do have some questions though.\"",
			"\"I see. I might need some clarification, though.\""
		];
	}

	// if current page is one of the help pages
	if (substr($option, 0, 3) == "hlp") {

		// sets specific "back" button content
		$backbtn = [
			"\"Lovely.\"",
			"\"Neat.\"",
			"\"Got it.\"",
			"\"A-OK.\"",
			"\"OHHH, OK.\"",
			"\"Fun!\"",
			"\"Cool cool.\"",
			"\"Gotcha.\"",
			"\"Ah...\""
		];
	}

	// if the page is the help hub
	if ($option == "hlp-init") {
		
		// sets specific "back" button content
		$backbtn = [
			"\"Nevermind.\"",
			"\"OK, I think I got it.\"",
			"\"I see...\"",
			"\"That's pretty much it then.\"",
			"\"It's a lot clearer now.\"",
			"\"OK, got it.\"",
			"\"OK, cool.\"",
			"\"I think I understand now.\"",
			"Understanding stare.",
			"You nod."
		];		
	}

    // checks the user's current stage
    switch ($stage)
    {
    	// if in stage 0, or tutorial
    	case 0:
    		
    		// sets content for the help hub
    		$hlplines = [
    			"\"I am all ears. Literally, all my components have aural recognition capabilities.\"",
    			"\"Ask away. Oh, I meant to say 'go away,' but whatever.",
    			"\"Let us hear those questions. Yes, let us. Yes, I also agree, let us.\"",
    		];

    		// checks the dialogue option picked by the user and determines dialogue and user's response options
    		switch ($option) {

    			default:
					$dialogue = "<p>The robot now abruptly changes its attitude. Its voice also changes, as if it was playing back a pre-recorded message. \"Welcome, Applicant, to Daimon Corporation International, where we strive to make the world a better place. Thank you for taking interest in a position at our company. The application process is simple: you just have to push a button several times and you will get the job.\"</p>\n<p>You raise your brows. \"A button?\" you think to yourself.</p>\n<p>The robot seems to read your thoughts.</p>\n<p>\"Yes, you heard it right. <strong>You only have to push a button. Some 1,000,000 times.</strong>\"</p>\n<p>You stand agape at the outrageous number.</p>\n<p>\"Fret not, though, as <strong>each button press will give you a point</strong>, which you can then use at the company <strong>Shop</strong> to purchase either <strong>decoration items</strong> that will inspire you to click the button more efficiently or <strong>body implants</strong> that will enhance your button-pressing capabilities.\"</p>\n<p>You do not like the sound of 'body implants', but you remain silent.</p>\n<p>\"You can place the decoration items in the <strong>button room</strong>, or check out which body implants you got by looking at yourself in the <strong>mirror</strong>.\" It makes a brief pause. \"You activity will be supervised by me, of course, the <strong>Supervision Associate</strong>, and by <strong>ABYSS</strong>, also known as the <strong>Advanced Button-Yield Surveillance System</strong>. You know, the one-eyed pyramid over there.\"</p>\n<p>You glance at the pyramid and shiver.</p>\n<p>The robot resumes its speech. \"Although you may not leave the facility until the Application Process is complete, you may take a break or <strong>sleep</strong> at any time.\" The robot's optic scrutinizes your confused expression. \"Is that clear?\"";
					responsegen($response, "continue", "\"Yeah, yeah, whatever. Let's get started.\"");
					responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
				break;

				case "continue":
					$dialogue = "\"Very well. Now, to demonstrate your understanding of the application process, I will lead you to the button, and you will have to <strong>press it 10 times</strong>. Once you get your first 10 Application Points, come back to see me, and I will tell you how to proceed.\"";
					query("UPDATE users SET stage = 1 WHERE id = ?", $_SESSION["id"]);
					responsegen($response, "redirect", "\"OK, let's do this.\"");
				break;
    		}

    	break;
   		
   		// if in stage 1
   		case 1:

   			// checks the dialogue option picked by the user and determines dialogue and user's response options
   			switch ($option) {

				default:
					
					// if user has less than the required 10 clicks
					if ($clicks < 10) {
						$dialogue = "<p>The Supervision Associate stares blankly at you.</p><p>\"It's not that difficult, you know? You only have to <strong>press the button 10 times</strong>.\"</p>";
	   					responsegen($response, "why", "\"Why do I have to do that to get the job?\"");
	   					responsegen($response, "times", "\"Why 10 times?\"");
	   					responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
   					}

   					// if the user has reached the required 10 clicks
   					else {
   						
   						// substracts the 10 clicks from the user
   						query("UPDATE users SET clicks = clicks - 10 WHERE id = ?", $_SESSION["id"]);
   						// updates stage
   						query("UPDATE users SET stage = 2 WHERE id = ?", $_SESSION["id"]);
   						$dialogue = "\"Well done. I'll take those 10 Application Points as proof that you completed your first Application Assignment.\"</p>\n<p>You give the Associate a sarcastic smile that goes completely unnoticed. Or ignored.</p>\n<p>\"Now, for your next assignment, you will have to prove that you understand the basics of the Shop. <strong>Go to the Shop and buy the Glass of Water, and then come back here</strong>.\"";
   						responsegen($response, "redirect", "\"Fine. BRB.\"");
   					}

				break;

				/* DIALOGUE BRANCHES */
				case "why":
					$dialogue = "\"It is vital to the success of our company that all our employees are proficient button-pushers.\"";
					responsegen($response, "default", "\"Right.\"");
				break;

				case "times":
					$dialogue = "\"Did you know that I am authorized to use deadly force at will?\"";
					responsegen($response, "default", "\"Fine, fine...\"");

				break;
    		}

   		break;

   		// if in stage 2
   		case 2:  		
   			
   			// checks the dialogue option picked by the user and determines dialogue and user's response options
   			switch ($option) {

   				default:
   					
   					// if the user does not possess the required item to progress
   					if (empty(query("SELECT time FROM buffs WHERE usrid = ? AND itmid = 1", $_SESSION["id"]))) {
   						$dialogue = "\"Just go to the Shop and <strong>buy the Glass of Water</strong>. It's not that expensive. Or is not irritating me too demanding a task for you?\"";

   						responsegen($response, "job", "\"What's the job about, anyway?\"");
   						responsegen($response, "others", "\"How many other Applicants are there?\"");
   						responsegen($response, "mad", "\"Are you mad about something?\"");
   						responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
   					}
   					
					// if the user does possess the required item to progress
   					else {
   						
   						// updates user's stage
   						query("UPDATE users SET stage = 3 WHERE id = ?", $_SESSION["id"]);
   						$dialogue = "\"Great! By pressing a button and with the points you got from it purchasing a glass of water, you have shown great resourcefulness. And resourcefulness is something greatly appreciated in prospective employees. Next, we want to make sure your that your commitment to our Company and your job is strong. For that, please go to the Shop and <strong>purchase the Robotic Leg implant</strong>. Come back when you have acquired it.\"";
   						responsegen($response, "redirect", "You don't feel comfortable about it, but really want the job.");

   					}
				break;

				/* DIALOGUE BRANCHES */
				
				case "job":
					$dialogue = "\"You will find out when... if you get it.\"";
					responsegen($response, "default", "\"Fair enough.\"");
				break;

				case "others":
					$dialogue = "\"Quite a few, so maybe you should stop talking to me and start doing what you should be doing.\"";
					responsegen($response, "default", "\"Point.\"");
				break;

				case "mad":
					$dialogue = "\"I am quite unable to experience feelings.\"";
					responsegen($response, "sorry", "\"Oh, sorry about that.\"");
				break;

				case "sorry":
					$dialogue = "\"It was insensitive from your part, but then again, I do not care.\"";
					responsegen($response, "silence", "Awkward silence.");
				break;

				/* EASTER EGG BRANCHES */

				case "silence":
					
					// check whether the user has already discovered the easter egg
					$check = query("SELECT bonus FROM users WHERE id = ?", $_SESSION["id"]);
					$check = $check[0]["bonus"];
					
					// if the user has not discovered the easter egg
					if ($check == 0) {
						$dialogue = "The Associate stares at you";
						responsegen($response, "silence2", "Stare back.");
						responsegen($response, "default", "\"Anyway...\"");
					}
					
					// if the user has already discovered the easter egg
					else {
						$dialogue = "Yeah, not again.";
						responsegen($response, "default", "\"Fine. It was worth a shot.\"");
					}
				break;

				case "silence2":
					$dialogue = "The lifeless optic does not move.";
					responsegen($response, "silence3", "You stay still.");
					responsegen($response, "default", "\"So...\"");
				break;

				case "silence3":
					$dialogue = "Absolutely nothing happens.";
					responsegen($response, "silence4", "You do nothing.");
					responsegen($response, "default", "\"Yeah...\"");
				break;

				case "silence4":
					$dialogue = "The silence remains.";
					responsegen($response, "silence5", "You embrace the silence.");
					responsegen($response, "default", "\"Um...\"");
				break;

				case "silence5":
					$dialogue = "The silence reminds you that you're only a blob of atoms whirling through an empty, meaningless void.";
					responsegen($response, "silence6", "You ponder the futility of existence.");
					responsegen($response, "default", "\"OK, that's enough.\"");
				break;

				case "silence6":
					$dialogue = "Oh, you don't want to go down that road.";
					responsegen($response, "silence7", "Try me.");
					responsegen($response, "default", "No, you're right. Take me back.");
				break;

				case "silence7":
					$dialogue = "You think of how the Sun will eventually engulf the Earth, effectively rendering all of human history pointless.";
					responsegen($response, "silence8", "Yes, I do.");
					responsegen($response, "default", "Make it stop.");
				break;

				case "silence8":
					$dialogue = "You think of how the Universe will one day come to an end, all matter ceasing to exist, as the forces that bond it together disperse.";
					responsegen($response, "silence9", "Indeed.");
					responsegen($response, "default", "PLEASE, JUST...");
				break;

				case "silence9":
					
					// gives the user 1000 clicks
					query("UPDATE users SET clicks = clicks + 1000 WHERE id = ?", $_SESSION["id"]);
					// updates the DB to reflect user has discovered the easter egg
					query("UPDATE users SET bonus = 1 WHERE id = ?", $_SESSION["id"]);
					$dialogue = "You... You... FINE! Here, have 1000 Application Points. Now, go away. I need to think about my life.";
					responsegen($response, "default", "Ooh! Thanks! I have no soul!");
				break;
   			}
   		break;

   		// if in stage 3
   		case 3:

   			// checks the dialogue option picked by the user and determines dialogue and user's response options
   			switch ($option) {
   				
   				default:
   					
   					// if the user does not possess the required item to progress
   					if (empty(query("SELECT time FROM buffs WHERE usrid = ? AND itmid = 4", $_SESSION["id"])) && empty(query("SELECT time FROM buffs WHERE usrid = ? AND itmid = 5", $_SESSION["id"]))) {
   						$dialogue = "\"What? Is modifying your body too much for you? Come on, humans get piercings done, tatoos, how is <strong>replacing your leg for a robotic one</strong> so terrible? Just go and do it.\"";
						responsegen($response, "necessary", "\"Is this really necessary?\"");
						responsegen($response, "others", "\"Did the other Applicants also have to go through this?\"");
   						responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
   					}
   					
   					// if the user does possess the required item to progress
   					else {

   						// updates user's stage
   						query("UPDATE users SET stage = 4 WHERE id = ?", $_SESSION["id"]);
   						$dialogue = "\"Congratulations, you are now ready for the final challenge. All you have to do is get 1,000,000 Applicant Points. Once you do, return to me to complete the Application process.\"";
   						responsegen($response, "redirect", "\"OK, time to prove my worth as a human by means of mindlessly pressing a button.\"");
   					}
				break;

				/* DIALOGUE BRANCHES */

				case "necessary":
					$dialogue = "\"The notion of 'necessariness' is a mere social construct.\"";
					responsegen($response, "word", "\"That's not even a word.\"");
					responsegen($response, "default", "\"I don't have time for this.\"");
				break;

				case "word":
					$dialogue = "\"The notion of 'word' is also a mere social construct.\"";
					responsegen($response, "default", "You sigh. \"Nevermind.\"");
				break;

				case "others":
					$dialogue = "\"The Application process is standardized, so, yes, all other Applicants have gone through this exact same process. And all future ones will have to do so too. Hopefully with less whining.\"";
					responsegen($response, "default", "\"Oh, shut up.\"");
				break;
   			}
   		break;

   		// if in stage 4
   		case 4:

   			// if user does not have the required amount of clicks to reach the endgame
   			if ($clicks < 1000000) {
				$dialogue = "\"Go on, the Button won't press itself. Until you <strong>get 1,000,000 Application Points</strong> you have no business being here.\"";
				responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
			}
			
			// if user has the required amount of clicks to reach the endgame
			else {
				
				// updates user's stage
				query("UPDATE users SET stage = 5 WHERE id = ?", $_SESSION["id"]);
				$dialogue = "\"Well, you made it. Congratulations. The job is yours. Now, follow me. I will brief you on the nature of your duties.\"";
				responsegen($response, "endgame", "\"About damn time, too.\"");
			}
   		break;

   		default:
   			$dialogue = "\"Shouldn't you be working right now?\"";
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;
    }
	
	// help page branches and options
	switch ($option) {

		case "hlp-init":
			$dialogue = $hlplines[array_rand($hlplines)];
			responseclr($response);
			responsegen($response, "hlp-button", "\"Tell me more about the button.\"");
			responsegen($response, "hlp-mirror", "\"Tell me more about the mirror.\"");
			responsegen($response, "hlp-room", "\"Tell me more about the room.\"");
			responsegen($response, "hlp-associate", "\"Tell me more about yourself.\"");
			responsegen($response, "hlp-abyss", "\"Tell me more about ABYSS.\"");
			responsegen($response, "hlp-shop", "\"Tell me more about the shop.\"");
			responsegen($response, "hlp-break", "\"Can I take breaks?\"");
			responsegen($response, "default", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-button":
			$dialogue = "\"It's just a button. A simple button. Which you have to press. Each time you press it, you gain a number of Applicant Point. You usually get one Application Point every time you press the button, but if you buy items at the Shop, you can get more Points per press. Get enough Application Points and you get the job. It's not that difficult.\"";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-mirror":
			$dialogue = "\"Seriously? You're asking me about a mirror?\" You feel like the Supervision Associate would sigh if it could. \"It works by directing your eyes towards it and interpreting the visual signals in your brain in an attempt to understand the meaning derived thereof. If you had any parts of your body modified at the Shop, you will see what they are and what they do. Remember, Body Implants grant you more points per button press.\" ";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-room":
			$dialogue = "\"It's a room. It exists in a dimension you can perceive and you can interact in several ways with it, such as existing in it, breathing the air contained in it, etc. You can also use it to place Decoration items bought at the Shop. Remember, Decoration items grant you an extra amount of points every X button presses.\"";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-associate":
			$dialogue = "\"Applicants are not allowed to ask questions to the Application Associate regarding the Application Associate. I'm here to guide you through the application process and provide helpful information. But already know that, given that you are abusing that functionality of mine.\"";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-abyss":
			$dialogue = "\"The Advanced Button-Yield Surveillance System is here to oversee the pressing of the button and its effects. Unfortunately, I am unable to provide further information on ABYSS or the Button, as it is classified. Feel free to approach ABYSS if you dare. WANT. If you want.\"";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-shop":
			$dialogue = "\"The Shop features several Decoration items and Body Implants that you can purchase in exchange for Application Points in order to reach the amount of Application Points required to get the job more rapidly and effectively. Naturally, you can also attempt to press the button 1,000,000 times. I'll be here watching. Of the two of us, I am the one who cannot die of old age.\"";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;

		case "hlp-break":
			$dialogue = "\"Well, you certainly can. Feel free to sleep whenever you feel like it. Remember that you will have to identify yourself every time to wake up, as I delete Applicants from my memory every couple of hours to preserve RAM.\"";
			responseclr($response);
			responsegen($response, "hlp-init", $backbtn[array_rand($backbtn)]);
		break;
	}

	// stores clicks, dialogue, and user responses in a variable to send back to JS
	$data = ["clicks" => $clicks, "dialogue" => $dialogue, "response" => $response];

	// if the option is redirect
	if ($option == "redirect") {

		// sets redirect flag to 1
		$data["redirect"] = 1;
	}

	// if the option is endgame
	if ($option == "endgame") {

		// sets endgame flag to 1
		$data["endgame"] = 1;
	}

	// outputs data as JSON (pretty-printed for debugging convenience)
    header("Content-type: application/json");
    print(json_encode($data, JSON_PRETTY_PRINT));
?>