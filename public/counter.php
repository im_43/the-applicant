<?php
	
	/* AJAX counter. This page provides the main functionality of the game,
	which is counting button clicks taking into account the user's buffs
	and updating the database accordingly.

	Returns JSON to the JavaScript that calls it.

	NOTE: It has been noted that the process produces a significant amount of lag
	when users have too many buffs for the page to compute rapidly
	into the count. There's still no solution for this issue. */
	
	// loads configuration
    require("../includes/config.php"); 

	// stores type of request
	$type = $_POST["type"];

	// initializes clicks variable
	$clicks = 0;

	// initializes the bonus variable with the default count of 1 point per click
	$bonus = 1;

	// gets the user's buffs
	$buffs = query("SELECT * FROM buffs WHERE usrid = ?", $_SESSION["id"]);

	// if the request is to load
	if ($type == "load") {

		// retrieves the users current amount of clicks
		$clicks = query("SELECT clicks FROM users WHERE id = ?", $_SESSION["id"]);
	}

	// if the request is to sum	
	if ($type == "sum") {
		
		// for each buff the user has
		foreach ($buffs as $buff) {

			// stores the id of the owned item and gets rid of empty array
			$item = query("SELECT * FROM shop WHERE id = ?", $buff["itmid"]);
			$item = $item[0];

			// updates the amount of consecutive clicks for the item in the DB
			query("UPDATE buffs SET offsetuses = offsetuses + 1 WHERE usrid = ? AND itmid = ?", $_SESSION["id"], $buff["itmid"]);
			
			// if consecutive clicks reaches the offset number for the item
			if ($item["offset"] <= $buff["offsetuses"]) {

				// adds buff to default amount of points per click
				$bonus += $item["amt"];
				// resets the consecutive clicks count
				query("UPDATE buffs SET offsetuses = 0 WHERE usrid = ? AND itmid = ?", $_SESSION["id"], $buff["itmid"]);
			}
		}

		// updates the clicks in the DB
		query("UPDATE users SET clicks = clicks + ? WHERE id = ?", $bonus, $_SESSION["id"]);
		// updates the clicks variable to send back to JS so it can be displayed
		$clicks = query("SELECT clicks FROM users WHERE id = ?", $_SESSION["id"]);
	}

	// outputs clicks as JSON (pretty-printed for debugging convenience)
    header("Content-type: application/json");
    print(json_encode($clicks, JSON_PRETTY_PRINT));

?>