<?php
	
	/* Controller for the endgame page. Has a system in place
	to prevent users from accessing it before they have actually
	reached the endgame scenario. */

	// loads configuration
	require("../includes/config.php");

	// if user reached page via GET (meaning they haven't been to the endgame yet)
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        
        // checks for current stage of the user
        $check = query("SELECT stage FROM users WHERE id = ?", $_SESSION["id"]);
        $check = $check[0]["stage"]; 
        
        // if the user has not reached the final stage
        if ($check < 5) {

			// redirect the user to prevent progression skipping
			redirect("/");
		}

	    // if the user has reached the final stage
	    else {

	    	// render the endgame page without the sidebar
			render("congratulations.php", 3, ["title" => "Congratulations"]);
		}
	}
    
    // else if user reached page via POST (meaning they come from the endgame page)
    else if ($_SERVER["REQUEST_METHOD"] == "POST") {

        // deletes the user from the DB and logs them out
        query("DELETE FROM buffs WHERE usrid = ?", $_SESSION["id"]);
        query("DELETE FROM users WHERE id = ?", $_SESSION["id"]);
        redirect("/logout.php");
    }

        /* Note: The only way to access this page via post is from the endgame page itself.
        Although this may sound a bit extreme, if the user has reached the endgame,
        as part of the narrative of the game, the user must be deleted so the game can restart
        and the plot makes sense.*/
?>