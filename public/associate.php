<?php
	
	/* Controller for Associate page. The associate page is the hub of game progression,
    although most of the actual content is handled via JS and AJAX.*/

    // loads configuration
    require("../includes/config.php"); 

    // retrieves user's current stage and gets rid of empty array
    $stage = query("SELECT stage FROM users WHERE id = ?", $_SESSION["id"]);
    $stage = $stage[0]["stage"];
    
    // if the stage is 0, renders tutorial page without sidebar
    if ($stage == 0) {
    	render("associate.php", 3, ["title" => "Welcome, Applicant", "stage" => $stage]);
    }

    // if the stage is not 0 renders regular associate page with sidebar
    else {
    	render("associate.php", 1, ["title" => "Supervision Associate", "stage" => $stage]);
    }
?>