<?php

	/* Controller page for Mirror, which is the UI for owned body-implant buffs */

	// loads configuration
	require("../includes/config.php");

	// initializes items variable
	$items = [];

	// gets user's buffs
	$buffs = query("SELECT * FROM buffs WHERE usrid = ?", $_SESSION["id"]);

	// initializes a generic counter
	$x = 0;

	// for each buff
	foreach ($buffs as $buff) {
		
		// get "body" type items into variable
		$row = query("SELECT * FROM shop WHERE id = ? AND type = \"body\"", $buff["itmid"]);

		// if there are, indeed, buffs at all
		if (!empty($row)) {

			// store the item information in variable and get rid of empty arrays
			$items[$x] = $row;
			$items[$x] = $items[$x][0]; 
			
			// increment counter
			$x++;
		}
	}

	// introduction dialogue array
	$intro = [
		"You look at yourself in the mirror",
		"Your eyes meet themselves in the mirror on the wall",
		"You glance at the mirror"
		];
	
	// empty inventory dialogue array
	$empty = [
		"and see nothing but a sad man.",
		"but see nothing."					
		];

	// renders mirror view, with the sidebar, passing dialogue and buff information
	render("/mirror.php", 1, ["title" => "Sad eyes look back", "items" => $items, "intro" => $intro[array_rand($intro)], "empty" => $empty[array_rand($empty)]]);

?>