<?php

	/* Controller page for Room, which is the UI for owned decoration buffs */
	
	// loads configuration
	require("../includes/config.php");

	// initializes items variable
	$items = [];

	// gets user's buffs
	$buffs = query("SELECT * FROM buffs WHERE usrid = ?", $_SESSION["id"]);

	// initializes a generic counter
	$x = 0;

	// for each buff
	foreach ($buffs as $buff) {
		
		// get "decoration" type items into variable
		$row = query("SELECT * FROM shop WHERE id = ? AND type = \"deco\"", $buff["itmid"]);

		// if there are, indeed buffs at all
		if (!empty($row)) {

			// store the item information in variable and get rid of empty arrays
			$items[$x] = $row;
			$items[$x] = $items[$x][0];

			// increment counter 
			$x++;
		}
	}

	// introduction dialogue array
	$intro = [
		"You look around",
		"You lift your sight",
		"Your eyes adjust to the darkness around you"
		];
	
	// empty inventory dialogue array
	$empty = [
		"and see an empty room.",
		"but see nothing."					
		];

	// renders room view, with the sidebar, passing dialogue and buff information
	render("/room.php", 1, ["title" => "The room stood perfectly still.", "items" => $items, "intro" => $intro[array_rand($intro)], "empty" => $empty[array_rand($empty)]]);

?>