<?php

    /* Controller page for login. Logs in existing users. */

    // loads configuration
    require("../includes/config.php"); 

    // if user reached page via GET
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        
        // renders form without sidebar
        render("login_form.php", 3, ["title" => "Identify yourself"]);
    }

    // else if user reached page via POST
    else if ($_SERVER["REQUEST_METHOD"] == "POST") {

        // queries database for user
        $rows = query("SELECT * FROM users WHERE users = ?", $_POST["username"]);

        // if we found user, check password
        if (count($rows) == 1) {
            
            // first (and only) row
            $row = $rows[0];

            // compares hash of user's input against hash that's in database
            if (crypt($_POST["password"], $row["hash"]) == $row["hash"]) {
                // remembers that user's now logged in by storing user's ID in session
                $_SESSION["id"] = $row["id"];

                // redirects to button
                redirect("/");
            }
        }

        // else redirects
        else {
            redirect("/");
        }
    }

?>
