<?php

    // loads configuration
    require("../includes/config.php"); 

    // logs out current user, if any
    logout();

    // redirects user
    redirect("/");

?>
