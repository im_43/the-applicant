<?php
	
	/* Controller for shop page. Handles purchases.*/

	// loads configuration
	require("../includes/config.php");

	// filter variables initialization
	$filter = "";
	$filterdb = "";

	// if there is a filter
	if (isset($_GET["f"])) {

		// store the filter
		$filter = $_GET["f"];
	}

	// depending on what the filter is, apply database filter
	switch ($filter) {

		case "decoration":
			$filterdb = "deco";
			break;
		case "implants":
			$filterdb = "body";
			break;
		// this is commented out for future implementation of passive items
		/* case "passive":
			$filterdb = "temp";
			break; */
	}

	// initialize purchase toggle variable
	$is_purchase = false;

	// if this is a purchase, set purchase toggle to true
	if (isset($_POST["purchase"])) {

		$is_purchase = $_POST["purchase"];
	}

	// get the initial clicks from the DB
	$clicks = query("SELECT clicks FROM users WHERE id = ?", $_SESSION["id"]);
	// get rid of empty arrays
	$clicks = $clicks[0]["clicks"];

	// if this is a purchase
	if ($is_purchase == true && $clicks >= $_POST["price"]) {

		// insert the purchase into the DB
		query("INSERT INTO buffs (usrid, itmid, time) VALUES (?, ?, NOW())", $_SESSION["id"], $_POST["id"]);
		
		// update user's clicks in DB
		query("UPDATE users SET clicks = clicks - ? WHERE id = ?", $_POST["price"], $_SESSION["id"]);

	}

	// if there is no filter
	if (empty($filter)) {

		// get all shop items from DB
		$items = query("SELECT * FROM shop ORDER BY price");
	}

	// if there is a filter
	else {

		// get only filtered shop items from DB
		$items = query("SELECT * FROM shop WHERE type = ? ORDER BY price", $filterdb);
	}

	// this variable is used below to insert the status key into the $items array
	$x = 0;

	// for each one of the items
	foreach ($items as $item) {

		// change the back-end name to a more front-end friendly name
		switch ($item["type"]) {
		case "deco":
			$item["type"] = "Decoration";
			break;
		case "body":
			$item["type"] = "Body Implant";
			break;
		case "temp":
			$item["type"] = "Passive Item";
			break;
		}

	/* Checks whether the item is owned by searching for the existence
	 * of a transaction timestamp on the DB
	 */
		$owned = query("SELECT time FROM buffs WHERE usrid = ? AND itmid = ?", $_SESSION["id"], $item["id"]);

		// if there is, indeed, a transaction of the user with the item
		if (!empty($owned)) {
			
			// declare this item as "owned"
			$item["status"] = "owned";
		}

		// if there is no transaction (therefore no timestamp)
		else {
		 	
		 	// declare this item as "not owned"
		 	$item["status"] = "notOwned";
		 }
	
		// adds the status to the items array
		$items[$x] = $item;
		
		// increments counter
		$x++;
	}
	
	// get the updated clicks from the DB
	$clicks = query("SELECT clicks FROM users WHERE id = ?", $_SESSION["id"]);
	// get rid of empty arrays
	$clicks = $clicks[0]["clicks"];

	// render the shop page, passing the title, the items info and the clicks info
	render("/shop.php", 1, ["title" => "Whaddya want?", "items" => $items, "clicks" => $clicks, "type" => $filter]);

?>	