<?php

    /**
     * functions.php
     *
     * The Applicant
     * by Lucas Traba
     * 
     * Helper functions.
     */

    // require the constants
    require_once(__DIR__ . "/constants.php");

    /**
     * Executes SQL statement, possibly with parameters, returning
     * an array of all rows in result set or false on (non-fatal) error.
     */
    function query(/* $sql [, ... ] */) {
        // SQL statement
        $sql = func_get_arg(0);

        // parameters, if any
        $parameters = array_slice(func_get_args(), 1);

        // try to connect to database
        static $handle;
        if (!isset($handle)) {
            try {
                // connect to database
                $handle = new PDO("mysql:dbname=" . DATABASE . ";host=" . SERVER, USERNAME, PASSWORD);

                // ensure that PDO::prepare returns false when passed invalid SQL
                $handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
            }
            catch (Exception $e) {
                trigger_error($e->getMessage(), E_USER_ERROR);
                exit;
            }
        }

        // prepare SQL statement
        $statement = $handle->prepare($sql);
        if ($statement === false) {
            trigger_error($handle->errorInfo()[2], E_USER_ERROR);
            exit;
        }

        // execute SQL statement
        $results = $statement->execute($parameters);
        if ($results === false) {
            trigger_error($statement->errorInfo()[2], E_USER_ERROR);
            exit;
        }

        // return result set's rows
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Renders template, passing in values.
     */
    function render($view, $mode, $values = []) {
        // if template exists, render it
        if (file_exists("../views/$view")) {
            // extract variables into local scope
            extract($values);

            // render header
            require("../views/header.php");

            // check is sidebar is required
            if ($mode == 1 || $mode == 2) {
                // render header
                require("../views/sidebar.php");
            }

            // check if UI header elements are required separately
            if ($mode == 1 || $mode == 3) {
                // render header
                require("../views/headerUI.php");
            }

            require("../views/headermid.php");

            // render template
            require("../views/$view");

            if ($mode == 1 || $mode == 2) {
                // render footer
                require("../views/footerUI.php");
            }

            // render footer
            require("../views/footer.php");
        }

        // else err
        else {
            trigger_error("Invalid view: $view", E_USER_ERROR);
        }
    }
    
    /**
     * Redirects user to destination, which can be
     * a URL or a relative path on the local host.
     *
     * Because this function outputs an HTTP header, it
     * must be called before caller outputs any HTML.
     */
    function redirect($destination) {
        // handle URL
        if (preg_match("/^https?:\/\//", $destination)) {
            header("Location: " . $destination);
        }

        // handle absolute path
        else if (preg_match("/^\//", $destination)) {
            $protocol = (isset($_SERVER["HTTPS"])) ? "https" : "http";
            $host = $_SERVER["HTTP_HOST"];
            header("Location: $protocol://$host$destination");
        }

        // handle relative path
        else {
            // adapted from http://www.php.net/header
            $protocol = (isset($_SERVER["HTTPS"])) ? "https" : "http";
            $host = $_SERVER["HTTP_HOST"];
            $path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
            header("Location: $protocol://$host$path/$destination");
        }

        // exit immediately since we're redirecting anyway
        exit;
    }

    /**
     * Logs out current user, if any.  Based on Example #1 at
     * http://us.php.net/manual/en/function.session-destroy.php.
     */
    function logout() {
        // unset any session variables
        $_SESSION = [];

        // expire cookie
        if (!empty($_COOKIE[session_name()])) {
            setcookie(session_name(), "", time() - 42000);
        }

        // destroy session
        session_destroy();
    }

    /**
     * Generates a response button, taking text and an option id as arguments.
     * It uses a memory address to push the button to the variable of the page
     * that's calling it
     */
    function responsegen(&$response, $id, $text) {
        $button = "<p><button type=\"button\" onclick=\"dialogue('". $id . "');\" class=\"drkbutton\">" . $text . "</button></p>";
        array_push($response, $button);
    }

    /*
     * Clears the whole response array the memory address.
     */
    function responseclr(&$response) {
        $response = [];
    }

?>
