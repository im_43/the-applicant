<?php

    /**
     * config.php
     *
     * The Applicant
     * by Lucas Traba
     *
     * Configures pages.
     */

    // display errors, warnings, and notices
    ini_set("display_errors", true);
    error_reporting(E_ALL);

    // requirements
    require(__DIR__ . "/constants.php");
    require(__DIR__ . "/functions.php");

    // enable sessions
    session_start();

    // require authentication for all pages except /login.php, /logout.php, /register.php, and "checker.php"
    if (!in_array($_SERVER["PHP_SELF"], ["/login.php", "/logout.php", "/register.php", "/checker.php"]))
    {
        if (empty($_SESSION["id"]))
        {
            redirect("login.php");
        }
    }

?>
